﻿using ElectionMVC.Data.Models;
using System.Collections.Generic;

namespace ElectionMVC.ViewModels
{
    public class ProposalListViewModel
    {
        public IEnumerable<Proposal> Proposals { get; set; }
    }
}
