﻿using ElectionMVC.Data.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElectionMVC.Controllers
{
    public class ElectionController
    {
        private readonly IElectionRepository _electionRepository;
        private readonly ILogger<ProposalController> _logger;

        public ElectionController(IElectionRepository electionRepository, ILogger<ProposalController> logger)
        {
            _electionRepository = electionRepository;
            _logger = logger;
        }

    }
}
