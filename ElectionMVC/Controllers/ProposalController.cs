﻿using ElectionMVC.Data.Interfaces;
using ElectionMVC.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
namespace ElectionMVC.Controllers
{
    /// <summary>
    /// This comment will be on swagger
    /// </summary>
    public class ProposalController:Controller
    {
        private readonly IElectionRepository _electionRepository;
        private readonly IProposalRepository _proposalRepository;
        private readonly ProposalListViewModel _proposalListViewModal;
        private readonly ILogger<ProposalController> _logger;

        public ProposalController(IElectionRepository electionRepository, IProposalRepository proposalRepository, 
            ILogger<ProposalController> logger)
        {
            _electionRepository = electionRepository;
            _proposalRepository = proposalRepository;
            _proposalListViewModal = new ProposalListViewModel();
            _logger = logger;
        }

        public ViewResult List()
        {
            _logger.LogInformation("Test first log");
            ViewBag.Name = "Test viewbag";
            _proposalListViewModal.Proposals = _proposalRepository.Proposals;
            return View(_proposalListViewModal);
        }
    }
}
