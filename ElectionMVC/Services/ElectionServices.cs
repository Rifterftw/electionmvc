﻿using ElectionMVC.Data;
using ElectionMVC.Data.Models;
using ElectionMVC.Services.ExceptionHandlers;
using Microsoft.EntityFrameworkCore;
using System;

namespace ElectionMVC.Services
{
    public class ElectionServices : ICRUDService<Election>
    {
        private readonly AppDbContext _context;
        public ElectionServices(AppDbContext context)
        {
            _context = context;
        }

        public void Create(Election entity, string userId)
        {
            var election = _context.Elections.Find(entity.Name);

            if (election != null)
            {
                throw new ValidationException($"{election.GetType()}-{election.Name} such election already exist", "");
            }
            Election newElection = new Election
            {
                Description = entity.Description,
                ImageUrl = entity.ImageUrl,
                Name = entity.Name,
                Proposals = entity.Proposals,
                CreatedBy = userId,
                Created = DateTime.Now
            };
            _context.Elections.Add(newElection);
            _context.SaveChanges();
        }

        public void Delete(Election entity, string userId)
        {
            var election = _context.Elections.Find(entity.Id);

            if (election == null)
            {
                throw new ValidationException($"{election.GetType()}-{election.Name} didn't find", "");
            }
            _context.Elections.Remove(election);
        }

        public Election Get(int Id, string userId)
        {
            var election = _context.Elections.Find(Id);

            if (election == null)
            {
                throw new ValidationException($"{election.GetType()}-{election.Name} didn't find", "");
            }
            return election;
        }

        public void Update(Election entity, string userId)
        {
            var election = _context.Elections.Find(entity.Id);

            if (election == null)
            {
                throw new ValidationException($"{election.GetType()}-{election.Name} didn't find", "");
            }
            election.LastModified = userId;
            election.Modified = DateTime.Now;
            _context.Entry(election).State = EntityState.Modified;
        }
    }
}
