﻿using ElectionMVC.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElectionMVC.Services
{
    public interface ICRUDService<T> where T:AuditableEntity
    {
        void Create(T entity, string userId);
        T Get(int Id, string userId);
        void Update(T entity, string userId);
        void Delete(T entity, string userId);
    }
}
