﻿using ElectionMVC.Data;
using ElectionMVC.Data.Models;
using ElectionMVC.Services.ExceptionHandlers;
using Microsoft.EntityFrameworkCore;
using System;

namespace ElectionMVC.Services
{
    public class ProposalServices:ICRUDService<Proposal>
    {
        private readonly AppDbContext _context;
        public ProposalServices(AppDbContext context)
        {
            _context = context;
        }
        public void Create(Proposal proposal, string userId)
        {
            var election = _context.Elections.Find(proposal.Election.Id);

            if(election == null)
            {
                throw new ValidationException($"{proposal.GetType()}-{proposal.Election.Name} didn't find", "");
            }
            Proposal newProposal = new Proposal
            {
                Count = 0,
                Description = proposal.Description,
                Name = proposal.Name,
                ImageUrl = proposal.ImageUrl,
                Election = proposal.Election,
                CreatedBy = userId,
                Created = DateTime.Now
            };
            _context.Proposals.Add(newProposal);
            _context.SaveChanges();
        }

        public Proposal Get(int id, string userId)
        {
            var proposal = _context.Proposals.Find(id);

            if(proposal == null)
            {
                throw new ValidationException($"{proposal.GetType()} {proposal.Election.Name} didn't find", "");
            }
            return proposal;
        }
        public void Update(Proposal proposal, string userId)
        {
            var checkProposal = _context.Proposals.Find(proposal.Id);

            if(checkProposal == null)
            {
                throw new ValidationException($"{proposal.GetType()} {proposal.Name} didn't find", "");
            }
            proposal.LastModified = userId;
            proposal.Modified = DateTime.Now;
            _context.Entry(proposal).State = EntityState.Modified;
        }

        public void Delete(Proposal proposal, string userId)
        {
            var checkProposal = _context.Proposals.Find(proposal.Id);

            if(checkProposal == null)
            {
                throw new ValidationException($"{proposal.GetType()} {proposal.Name} didn't find", "");
            }
            _context.Proposals.Remove(proposal);
        }
    }
}
