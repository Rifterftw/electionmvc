﻿using ElectionMVC.Data.Interfaces;
using ElectionMVC.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElectionMVC.Data.Repositories
{
    public class ElectionRepository : IElectionRepository
    {
        private readonly AppDbContext _appDbContext;
        public ElectionRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public IEnumerable<Election> Elections => _appDbContext.Elections.Include(c => c.Proposals);
        public Election GetById(int id) => _appDbContext.Elections.FirstOrDefault(c => c.Id == id);
    }
}
