﻿using ElectionMVC.Data.Interfaces;
using ElectionMVC.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElectionMVC.Data.Repositories
{
    public class ProposalRepository : IProposalRepository
    {
        private readonly AppDbContext _appDbContext;
        public ProposalRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Proposal> Proposals => _appDbContext.Proposals.Include(c => c.Election);
    }
}
