﻿using ElectionMVC.Data.Models;
using System.Collections.Generic;

namespace ElectionMVC.Data.Interfaces
{
    public interface IProposalRepository
    {
        IEnumerable<Proposal> Proposals { get; }
    }
}
