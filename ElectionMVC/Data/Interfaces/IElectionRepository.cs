﻿using ElectionMVC.Data.Models;
using System.Collections.Generic;

namespace ElectionMVC.Data.Interfaces
{
    public interface IElectionRepository
    {
        IEnumerable<Election> Elections { get; }

        Election GetById(int id);
    }
}
