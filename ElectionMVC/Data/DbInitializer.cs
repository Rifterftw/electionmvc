﻿using ElectionMVC.Data.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;

namespace ElectionMVC.Data
{
    public class DbInitializer{
    public static void Seed(AppDbContext context)
    {
        if (!context.Elections.Any())
        {
            context.Elections.AddRange(Elections.Select(c => c.Value));
        }

        if (!context.Proposals.Any())
        {
            context.AddRange
            (
                new Proposal
                {
                    Name = "Beer",
                    Count=1,
                    Description="First car test string",
                    Election=Elections["Car"]
                },
                new Proposal
                {
                    Name = "Some",
                    Count = 3,
                    Description = "Second car test string",
                    Election = Elections["Car"]
                },
                new Proposal
                {
                    Name = "BMW",
                    Count = 11,
                    Description = "Third car test string",
                    Election = Elections["Car"]
                },
                new Proposal
                {
                    Name = "Kee",
                    Count = 8,
                    Description = "First travel string",
                    Election = Elections["Traveling"]
                },
                new Proposal
                {
                    Name = "Bee",
                    Count = 3,
                    Description = "Second travel string",
                    Election = Elections["Traveling"]
                }
            );
        }

        context.SaveChanges();
    }

    private static Dictionary<string, Election> elections;
    public static Dictionary<string, Election> Elections
    {
        get
        {
            if (elections == null)
            {
                var electionList = new Election[]
                {
                        new Election { Name = "Car", Description="To buy a car" },
                        new Election { Name = "Traveling", Description="Move to Estonia" }
                };

                    elections = new Dictionary<string, Election>();

                foreach (Election election in electionList)
                {
                    elections.Add(election.Name, election);
                }
            }

            return elections;
        }
    }
}
}