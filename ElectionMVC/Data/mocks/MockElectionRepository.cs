﻿using System.Collections.Generic;
using ElectionMVC.Data.Interfaces;
using ElectionMVC.Data.Models;

namespace ElectionMVC.Data.mocks
{
    public class MockElectionRepository : IElectionRepository
    {
        public IEnumerable<Election> Elections
        {
            get
            {
                return new List<Election>
                {
                    new Election
                    {
                        Id=1,
                        Name="test election",
                        Description="Test test",
                        ImageUrl="none"
                    }
                };
            }
            set { }
        }

        public Election GetById(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
