﻿using System.Collections.Generic;
using ElectionMVC.Data.Interfaces;
using ElectionMVC.Data.Models;

namespace ElectionMVC.Data.mocks
{
    public class MockProposalRepository : IProposalRepository
    {
        public IEnumerable<Proposal> Proposals
        {
            get
            {
                return new List<Proposal>
                {
                    new Proposal
                    {
                        Id=1,
                        Count=5,
                        Description="To buy a new car",
                        Name="Car",
                        Election=new Election
                        {
                            Id = 1,
                            Description="Some test",
                            Name="Tested name",
                            ImageUrl="none"
                        }
                    }
                };
            }
            set { }
        }
    }
}
