﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElectionMVC.Data.Models
{
    public class Election: AuditableEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Proposal> Proposals { get; set; }
        public string ImageUrl { get; set; }
    }
}
